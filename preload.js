const child_process = require('child_process');

window.addEventListener('DOMContentLoaded', () => {
    /*document.getElementById("quit-button").addEventListener('click', () => {
        app.quit();
    });*/

    document.getElementById("play-button").addEventListener('click', () => {
        console.log("Mode: " + document.getElementById("mode").value);

        var args = [];
        switch (document.getElementById("mode").value) {
            case "create":
                args = [
                    "-host",
                    document.getElementById("player-count").value,
                    "-port",
                    document.getElementById("port").value,
                    "+map",
                    document.getElementById("map").value,
                ]
                break;

            case "join":
                args = ["-join", document.getElementById("ip").value]
                break;

            default:
                alert("Error: invalid operation");
                break;
        }

        console.log("Launching GZDoom...");
        const gzdoom = child_process.spawn('gzdoom', args);
        gzdoom.stdout.on('data', (data) => {
            console.log(`${data}`);
        });

        gzdoom.stderr.on('data', (data) => {
            console.log(`${data}`);
        });

        gzdoom.on('close', (code) => {
            console.log(`Exit code ${code}`);
        });
    });
})